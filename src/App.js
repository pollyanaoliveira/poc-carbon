import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.scss';
import TopHeader from './components/TopHeader/TopHeader';
import Main from './content/Main';
import TablesDetails from './content/TablesDetails';
import Report from './content/Report/Report';

function App() {
  return (
    <>
      <TopHeader />
      <Switch>
        <Route path="/administracao" component={Report} />
        <Route path="/relatorios" component={TablesDetails} />
        <Route path="/usuarios" component={TablesDetails} />
        <Route path="/conteudos" component={TablesDetails} />
        <Route path="/eventos" component={TablesDetails} />
        <Route exact path="/" component={Main} />
      </Switch>
    </>
  );
}

export default App;
