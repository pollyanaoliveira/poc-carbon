import React from 'react';
import { AppSwitcher20, Notification20 } from '@carbon/icons-react';
import { HeaderGlobalBar, HeaderGlobalAction } from 'carbon-components-react';
import Login from './Login/Login';

const GlobalIcons = () => {
  return (
    <HeaderGlobalBar>
      <HeaderGlobalAction aria-label="Notifications">
        <Notification20 />
      </HeaderGlobalAction>
      <HeaderGlobalAction aria-label="User Avatar">
        <Login />
      </HeaderGlobalAction>
      <HeaderGlobalAction aria-label="App Switcher">
        <AppSwitcher20 />
      </HeaderGlobalAction>
    </HeaderGlobalBar>
  );
};

export default GlobalIcons;
