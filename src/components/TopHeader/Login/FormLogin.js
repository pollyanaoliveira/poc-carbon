import React from 'react';
import useLogin from '../Talons/useLogin';
import { FormGroup, TextInput } from 'carbon-components-react';

const FormLogin = ({ InvalidPasswordProps }) => {
  // const { handleChange } = useLogin();
  return (
    <>
      <FormGroup className="test" style={{ maxWidth: '400px' }}>
        <TextInput labelText="Email" style={{ marginBottom: '1rem' }} />
        <TextInput
          // onChange={handleChange}
          type="password"
          required
          pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
          {...{ ...InvalidPasswordProps }}
        />
      </FormGroup>
    </>
  );
};

export default FormLogin;
