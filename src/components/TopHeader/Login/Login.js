import { useState } from 'react';
import { createPortal } from 'react-dom';
import { UserAvatar20 } from '@carbon/icons-react';
import { Modal } from 'carbon-components-react';
import FormLogin from './FormLogin';

const UserGlobal = () => {
  const ModalStateManager = ({
    renderLauncher: LauncherContent,
    children: ModalContent
  }) => {
    const [open, setOpen] = useState(false);
    return (
      <>
        {!ModalContent || typeof document === 'undefined'
          ? null
          : createPortal(<ModalContent open={open} setOpen={setOpen} />, document.body)}
        {LauncherContent && <LauncherContent open={open} setOpen={setOpen} />}
      </>
    );
  };
  return (
    <ModalStateManager
      renderLauncher={({ setOpen }) => (
        <UserAvatar20 onClick={() => setOpen(true)}></UserAvatar20>
      )}
    >
      {({ open, setOpen }) => (
        <Modal
          modalHeading="Login"
          primaryButtonText="Entrar"
          secondaryButtonText="Cancelar"
          open={open}
          onRequestClose={() => setOpen(false)}
        >
          <FormLogin />
        </Modal>
      )}
    </ModalStateManager>
  );
};

export default UserGlobal;
