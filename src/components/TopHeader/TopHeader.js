import React from 'react';
import { Link } from 'react-router-dom';
import {
  Header,
  HeaderContainer,
  HeaderName,
  HeaderNavigation,
  HeaderMenuButton,
  HeaderMenuItem,
  HeaderGlobalBar,
  SkipToContent,
  SideNav,
  SideNavItems,
  HeaderSideNavItems,
  SideNavMenu,
  SideNavMenuItem
} from 'carbon-components-react';
import HeaderIcons from './HeaderIcons';

const TopHeader = () => (
  <HeaderContainer
    render={({ isSideNavExpanded, onClickSideNavExpand }) => (
      <Header aria-label="NewDash">
        <SkipToContent />
        <HeaderMenuButton
          aria-label="Open menu"
          onClick={onClickSideNavExpand}
          isActive={isSideNavExpanded}
        />
        <HeaderName element={Link} to="/" prefix="S2G">
          NewDash
        </HeaderName>
        <HeaderNavigation aria-label="NewDash">
          <HeaderMenuItem element={Link} to="/administracao">
            Administração
          </HeaderMenuItem>
          <HeaderMenuItem element={Link} to="/servicos">
            Serviços
          </HeaderMenuItem>
          <HeaderMenuItem element={Link} to="/privacidade">
            Política de Privacidade
          </HeaderMenuItem>
          <HeaderMenuItem element={Link} to="/sair">
            Log Out
          </HeaderMenuItem>
        </HeaderNavigation>
        <SideNav
          aria-label="Side navigation"
          expanded={isSideNavExpanded}
          isPersistent={false}
        >
          <SideNavItems>
            <HeaderSideNavItems>
              <HeaderMenuItem element={Link} to="/home">
                Home
              </HeaderMenuItem>
              <HeaderMenuItem element={Link} to="/administracao">
                Administração
                <SideNavMenu title="Área Administrativa">
                  <SideNavMenuItem element={Link} to="/relatorios">
                    Relatórios
                  </SideNavMenuItem>
                  <SideNavMenuItem element={Link} to="/users">
                    Usuários
                  </SideNavMenuItem>
                  <SideNavMenuItem element={Link} to="/conteudos">
                    Conteúdos
                  </SideNavMenuItem>
                  <SideNavMenuItem element={Link} to="/eventos">
                    Eventos
                  </SideNavMenuItem>
                </SideNavMenu>
              </HeaderMenuItem>
              <HeaderMenuItem element={Link} to="/servicos">
                Serviços
              </HeaderMenuItem>
              <HeaderMenuItem element={Link} to="/privacidade">
                Política de Privacidade
              </HeaderMenuItem>
              <HeaderMenuItem element={Link} to="/sair">
                Log Out
              </HeaderMenuItem>
            </HeaderSideNavItems>
          </SideNavItems>
        </SideNav>
        <HeaderGlobalBar />
        <HeaderIcons />
      </Header>
    )}
  />
);

export default TopHeader;
