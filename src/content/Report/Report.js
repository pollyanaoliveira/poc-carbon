import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button } from 'carbon-components-react';
import './Report.scss';

const Report = () => {
  const history = useHistory();

  return (
    <div className="root">
      <Button onClick={() => history.push('./relatorios')}>Relatórios</Button>
      <Button onClick={() => history.push('./usuarios')}>Usuários</Button>
      <Button onClick={() => history.push('./conteudos')}>Conteúdos</Button>
      <Button onClick={() => history.push('./eventos')}>Eventos</Button>
    </div>
  );
};

export default Report;
