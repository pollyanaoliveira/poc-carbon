import { Fragment } from 'react';
import './Tables';
import {
  DatePickerInput,
  DatePicker,
  DataTable,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableExpandHeader,
  TableHeader,
  TableBody,
  TableExpandRow,
  TableCell,
  TableExpandedRow
} from 'carbon-components-react';

const Tables = ({ rows, headers }) => {
  return (
    <Fragment>
      <DataTable
        rows={rows}
        headers={headers}
        render={({ rows, headers, getHeaderProps, getRowProps, getTableProps }) => (
          <TableContainer
            title="Carbon Repositories"
            description="Relatórios Administrativos"
          >
            <DatePicker datePickerType="range">
              <DatePickerInput
                id="date-picker-input-id-start"
                placeholder="mm/dd/yyyy"
                labelText="Data de Início"
              />
              <DatePickerInput
                id="date-picker-input-id-finish"
                placeholder="mm/dd/yyyy"
                labelText="Data de fim"
              />
            </DatePicker>
            <Table {...getTableProps()}>
              <TableHead>
                <TableRow>
                  <TableExpandHeader />
                  {headers.map((header) => (
                    <TableHeader {...getHeaderProps({ header })}>
                      {header.header}
                    </TableHeader>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <Fragment key={row.id}>
                    <TableExpandRow {...getRowProps({ row })}>
                      {row.cells.map((cell) => (
                        <TableCell key={cell.id}>{cell.value}</TableCell>
                      ))}
                    </TableExpandRow>
                    <TableExpandedRow colSpan={headers.length + 1}>
                      <p>Row description</p>
                    </TableExpandedRow>
                  </Fragment>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )}
      />
    </Fragment>
  );
};

export default Tables;
