import React, { Fragment } from 'react';
import { useHistory } from 'react-router-dom';
import './TablesDetails.scss';
import useTables from './Talons/useTables';
import { Button } from 'carbon-components-react';
import Tables from './Tables';

const TablesDetails = () => {
  const history = useHistory();
  const { headers, rows } = useTables();

  return (
    <Fragment>
      <Tables headers={headers} rows={rows} />
      <div className="return_button">
        <Button
          style={{
            width: '10rem'
          }}
          onClick={() => history.push('./administracao')}
        >
          Voltar
        </Button>
      </div>
    </Fragment>
  );
};

export default TablesDetails;
